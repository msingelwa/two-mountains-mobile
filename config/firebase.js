
import 'firebase/auth';
import Constants from 'expo-constants';
import firebase from "firebase/compat";
// import firebase from "firebase"
import "firebase/firestore"

const config = {
    apiKey: "AIzaSyDkSFs3gjXwdX4s-5gxGoUMlujtz-jFK2A",
    authDomain: "two-mountains-24d72.firebaseapp.com",
    databaseURL: "https://two-mountains-24d72-default-rtdb.firebaseio.com",
    projectId: "two-mountains-24d72",
    storageBucket: "two-mountains-24d72.appspot.com",
    messagingSenderId: "740481691299",
    appId: "1:740481691299:web:ba4936a1b806f235e3d916"
}



// Initialize Firebase
const firebaseConfig = {
    databaseURL: Constants.manifest.extra.databaseUrl,
    apiKey: Constants.manifest.extra.apiKey,
    authDomain: Constants.manifest.extra.authDomain,
    projectId: Constants.manifest.extra.projectId,
    storageBucket: Constants.manifest.extra.storageBucket,
    messagingSenderId: Constants.manifest.extra.messagingSenderId,
    appId: Constants.manifest.extra.appId
};

let Firebase;

// if (firebase.apps.length === 0) {
    firebase.initializeApp(config);
// }
export const firestore = firebase.firestore;
export default firebase;
