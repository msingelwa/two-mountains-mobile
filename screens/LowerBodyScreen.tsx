import React, {Component, useState} from 'react';
import {ActivityIndicator, AsyncStorage, ScrollView, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import RadioGroup, {RadioButtonProps} from "react-native-radio-buttons-group/lib/index";
import {radioButtonsData} from "../data/dummy";

function LowerBodyScreen({navigation, route } : any) {

    const [radioButtons, setRadioButtons] = useState<RadioButtonProps[]>(radioButtonsData)
    const [radioButtons1, setRadioButtons1] = useState<RadioButtonProps[]>(radioButtonsData)
    const [radioButtons2, setRadioButtons2] = useState<RadioButtonProps[]>(radioButtonsData)
    const [radioButtons3, setRadioButtons3] = useState<RadioButtonProps[]>(radioButtonsData)
    const [radioButtons4, setRadioButtons4] = useState<RadioButtonProps[]>(radioButtonsData)
    const [radioButtons5, setRadioButtons5] = useState<RadioButtonProps[]>(radioButtonsData)
    const [isLoading, setLoading] = useState(false);

    let leftThigh: any;
    let rightThigh: any;
    let leftLeg : any;
    let RightLeg : any;
    let rightFoot: any;
    let leftFoot: any;

    const lower = route.params.data;

    function onPressRadioButton1(radios: RadioButtonProps[]) {
        radioButtons.map((m, i) => {
            if (m.selected){
                leftThigh = m.value;
            }
        })
    }function onPressRadioButton2(radios: RadioButtonProps[]) {
        radioButtons1.map((m, i) => {
            if (m.selected){
                rightThigh = m.value
            }
        })
    }function onPressRadioButton3(radios: RadioButtonProps[]) {
        radioButtons2.map((m, i) => {
            if (m.selected){
                leftLeg = m.value
            }
        })
    }function onPressRadioButton4(radios: RadioButtonProps[]) {
        radioButtons3.map((m, i) => {
            if (m.selected){
                RightLeg = m.value
            }
        })
    }function onPressRadioButton5(radios: RadioButtonProps[]) {
        radioButtons4.map((m, i) => {
            if (m.selected){
                leftFoot = m.value
            }
        })
    }function onPressRadioButton6(radios: RadioButtonProps[]) {
        radioButtons4.map((m, i) => {
            if (m.selected){
                rightFoot = m.value
            }
        })
    }

    const handleSubmit = async () => {

        const data : any =  {
            leftThigh,
            rightThigh,
            leftLeg,
            RightLeg,
            rightFoot,
        }
        try {
            const newCollection = {...lower, ...data};
            await AsyncStorage.setItem('collection', JSON.stringify(data));
            navigation.navigate('Location', {data: newCollection});
            setLoading(false);
        } catch (error) {
            // Error saving data
            console.log(error)
        }
    }

    return (
        <>
            <Text style={styles.text}>Lower Body</Text>
        <ScrollView>
            <View style={styles.container}>
                <View style={styles.items}>
                    <Text style={styles.text}>Left Thigh</Text>
                    <Text style={styles.text}>Right Thigh</Text>
                    <Text style={styles.text}>Left Leg</Text>
                    <Text style={styles.text}>Right Leg</Text>
                    <Text style={styles.text}>Right Foot</Text>
                    <Text style={styles.text}>Left Foot</Text>
                </View>
                <View style={styles.items}>
                    <View style={styles.inside}>
                        <View style={[styles.item]}>
                            <RadioGroup

                                layout={"row"}
                                radioButtons={radioButtons}
                                onPress={value => onPressRadioButton1(value)}
                            />
                        </View>
                    </View>
                    <View style={styles.inside}>
                        <View style={[styles.item]}>
                            <RadioGroup
                                layout={"row"}
                                radioButtons={radioButtons1}
                                onPress={value => onPressRadioButton2(value)}
                            />
                        </View>
                    </View>
                    <View style={styles.inside}>
                        <View style={[styles.item]}>
                            <RadioGroup
                                layout={"row"}
                                radioButtons={radioButtons2}
                                onPress={value => onPressRadioButton3(value)}
                            />
                        </View>
                    </View><View style={styles.inside}>
                    <View style={[styles.item]}>
                        <RadioGroup
                            layout={"row"}
                            radioButtons={radioButtons3}
                            onPress={value => onPressRadioButton4(value)}
                        />
                    </View>
                </View>
                    <View style={styles.inside}>
                    <View style={[styles.item]}>
                        <RadioGroup
                            layout={"row"}
                            radioButtons={radioButtons4}
                            onPress={value => onPressRadioButton5(value)}
                        />
                    </View>
                </View><View style={styles.inside}>
                    <View style={[styles.item]}>
                        <RadioGroup
                            layout={"row"}
                            radioButtons={radioButtons5}
                            onPress={value => onPressRadioButton6(value)}
                        />
                    </View>
                </View>
                </View>
                <TouchableOpacity style={styles.button} onPress={() => handleSubmit()}>
                    <Text style={styles.buttonText}>Next</Text>
                </TouchableOpacity>
                {
                    isLoading ? (
                        <View style={styles.loader}>
                            <ActivityIndicator size={"large"} color="#777535"/>
                        </View>
                    ) : undefined
                }
            </View>
        </ScrollView>
        </>
    );
}

const styles = StyleSheet.create({
    loader: {
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
    },
    container: {
        flex: 1,
        marginTop: 10,
        flexDirection: 'row',
        flexWrap: 'wrap',
        // alignItems: 'center',
        paddingLeft: 20,
        backgroundColor: "#ffffff"
    },
    inside: {
        flex: 2,
        flexDirection: 'row'
    },
    items: {
        width: "30%"
    },item: {
        width: "60%",
        paddingHorizontal: 10
    },
    button: {
        backgroundColor: '#D4AF37',
        borderRadius: 5,
        height: 45,
        width: "100%",
        marginTop: 5,
        marginBottom: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonText: {
        color: '#FFF',
        fontWeight: 'bold',
        fontSize: 16,
    },
    text: {
        padding: 20,
        fontSize: 20,
    }
});

export default LowerBodyScreen;
