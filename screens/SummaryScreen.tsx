import React, {useEffect, useState} from 'react';
import {ActivityIndicator, Alert, AsyncStorage, Button, Platform, ToastAndroid, TouchableOpacity} from 'react-native';
import {Text, View, StyleSheet} from "react-native";
import firebase from "../config/firebase";
import { getDatabase, ref, onValue, set, push } from 'firebase/database';
import Collection from "../models/collection";
import {Card, Icon} from "react-native-elements";
function SummaryScreen({navigation, route }: any) {
    const [isLoading, setIsLoading] = useState(false);
    const newCollection = route.params.data;

    const db = getDatabase();
    const reference = ref(db, 'collections/');

    // const myObject  = JSON.parse(newCollection.Object);

    console.log("Object");
    // console.log(myObject);
    console.log(newCollection);
    console.log("Values");
    console.log(newCollection.Chest);

    let value = {};
    let head = {};
    let upper = {}
    let lower = {};
    let location = {};

    const submit = async () => {
        setIsLoading(true);
        // const collection = JSON.stringify(newCollection);
        const {
            Chest,
            LeftArm,
            LeftHand,
            RightArm,
            RightHand,
            RightLeg,
            collectedFrom ,
            ears,
            eyes,
            informant,
            informantNumber,
            leftLeg,
            leftThigh,
            mouth,
            name,
            nose,
            rightFoot,
            rightThigh,
            idNo,
            surname,
            timeNow,
        } = newCollection;

        await push(reference, {
            Chest,
            LeftArm,
            LeftHand,
            RightArm,
            RightHand,
            RightLeg,
            collectedFrom ,
            ears,
            eyes,
            informant,
            informantNumber,
            leftLeg,
            leftThigh,
            mouth,
            name,
            nose,
            rightFoot,
            rightThigh,
            idNo,
            surname,
            timeNow,
        }).then(res => {
            Alert.alert('CollectionScreen', 'Successfully Added Collection');

            navigation.navigate('Collection');
        }).catch(err => {
            Alert.alert("Error " + err);
        }).finally(() => {
            setIsLoading(false);
        });
    }

    const handleSubmit = () => {
        // navigation.navigate('Home');
        if (Platform.OS === 'android') {
            submit()
            ToastAndroid.show("Successfully submitted", ToastAndroid.SHORT)
        } else {
            submit()
            Alert.alert("Successfully submitted");
        }
    }
        return (
            <View style={{justifyContent: 'center', }}>
                <Card>
                    <Card.Title>Collection</Card.Title>
                    <Card.Divider />

                            <View style={styles.summary}>
                                {/*<Icon name={'info'} size={30} />*/}
                                    <Text style={styles.name}>Name: {newCollection.name}</Text>
                                    <Text style={styles.name}>Surname: {newCollection.surname}</Text>
                                    <Text style={styles.name}>Id No: {newCollection.idNo}</Text>
                                    <Text style={styles.name}>Collected From: {newCollection.collectedFrom}</Text>
                                    <Text style={styles.name}>Informant: {newCollection.informant}</Text>
                                    <Text style={styles.name}>Informant No: {newCollection.informantNumber}</Text>
                                    <Text style={styles.name}>Time: {newCollection.timeNow}</Text>
                            </View>
                </Card>
                <TouchableOpacity style={styles.button} onPress={submit}>
                    <Text style={styles.buttonText}>Submit</Text>
                </TouchableOpacity>
                {
                    isLoading ? (
                        <View style={styles.loader}>
                            <ActivityIndicator size={"large"} color="#777535"/>
                        </View>
                    ) : undefined
                }
            </View>
        );
}

const styles = StyleSheet.create({
    loader: {
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
    },
    button: {
        backgroundColor: '#D4AF37',
        borderRadius: 5,
        height: 45,
        width: "80%",
        marginTop: 5,
        marginLeft: 35,
        marginRight: 35,
        marginBottom: 10,
        paddingHorizontal: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonText: {
        color: '#FFF',
        fontWeight: 'bold',
        fontSize: 16,
    },
    summary: {
        flexDirection: 'column',
        marginBottom: 6,

    },
    name: {
        fontSize: 16,
        marginTop: 10
    },
})
export default SummaryScreen;
