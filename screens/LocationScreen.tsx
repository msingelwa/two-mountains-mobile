import React, {useState} from 'react';
import {
    AsyncStorage,
    Picker,
    Platform,
    SafeAreaView,
    StatusBar,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View
} from "react-native";
import DateTimePicker from '@react-native-community/datetimepicker';


function LocationScreen({navigation, route}: any) {
    const data = route.params.data;
    const [time, setTime] = useState(new Date());
    const [collectedFrom, setCollectedFrom] = useState("");

    const [date, setDate] = useState(new Date(1598051730000));
    const [mode, setMode] = useState('date');
    const [show, setShow] = useState(false);

    const onChange = (event: any, selectedDate: any) => {
        const currentDate = selectedDate || date;
        setShow(Platform.OS === 'ios');
        setDate(currentDate);
        setTime(currentDate)
    };

    const showMode = (currentMode:string) => {
        setShow(true);
        setMode(currentMode);
    };

    const showDatepicker = () => {
        showMode('date');
    };

    const showTimepicker = () => {
        showMode('time');
    };

    const storeData = async () => {
        const location : any =  {
            collectedFrom,
            // time,
        }
        try {
            await AsyncStorage.setItem('location', JSON.stringify(location));
            let loc = {...data, ...location}
            navigation.navigate("Signature", {data: loc});
        } catch (error) {
            // Error saving data
            console.log(error)
        }
    }


    function handleSubmit() {
        navigation.navigate("Summary")
    }

    return (
        <SafeAreaView style={{flex: 1, position: 'relative'}}>
            <StatusBar barStyle="dark-content" backgroundColor="#2f95dc" animated={true}/>
            <View style={styles.container}>
                {/*<TextInput*/}
                {/*    style={styles.textInput}*/}
                {/*    placeholder="Drop of Location"*/}
                {/*    keyboardType="default"*/}
                {/*    returnKeyType="done"*/}
                {/*    blurOnSubmit*/}
                {/*    onChangeText={text => setName(text)}*/}
                {/*    value={name}*/}
                {/*/>*/}

                <View style={styles.textInput}>
                    <Picker
                        selectedValue={collectedFrom}
                        onValueChange={value => setCollectedFrom(value)}>
                        <Picker.Item label="Going to which Mortuary" value="" />
                        <Picker.Item label="Mortuary 1" value="Mortuary 1" />
                        <Picker.Item label="Mortuary 2" value="Mortuary 2" />
                        <Picker.Item label="Mortuary 3" value="Mortuary 3" />
                        <Picker.Item label="Mortuary 4" value="Mortuary 4" />
                    </Picker>
                </View>

                {/*<TextInput*/}
                {/*    style={styles.textInput}*/}
                {/*    placeholder="Going to Which Mortuary"*/}
                {/*    keyboardType="default"*/}
                {/*    returnKeyType="done"*/}
                {/*    blurOnSubmit*/}
                {/*    onChangeText={text => setCollectedFrom(text)}*/}
                {/*    value={collectedFrom}*/}
                {/*/>*/}
                <View>
                {/*<Button onPress={showTimepicker} title="Select time collection" />*/}
            </View>
                {/*{show && (*/}
                {/*    <DateTimePicker*/}
                {/*        testID="dateTimePicker"*/}
                {/*        value={date}*/}
                {/*        mode="time"*/}
                {/*        is24Hour={true}*/}
                {/*        display="clock"*/}
                {/*        onChange={onChange}*/}
                {/*    />*/}
                {/*)}*/}
                <View
                    style={[styles.textInput, {justifyContent: 'center', alignContent: 'center', }]}
                    // onChangeText={text => setTime(text)}

                ><Text style={{justifyContent: 'center', alignContent: 'center', }}>{time.toLocaleTimeString()}</Text></View>


                <TouchableOpacity style={styles.button} onPress={storeData}>
                    <Text style={styles.buttonText}>Next</Text>
                </TouchableOpacity>

                {/*<TouchableOpacity style={styles.button} onPress={resetForm}>*/}
                {/*    <Text style={styles.buttonText}>Reset</Text>*/}
                {/*</TouchableOpacity>*/}
            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        top: 25,
        alignItems: 'center',
        justifyContent: 'center',
        // paddingHorizontal: 5
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    collected: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'flex-start',
        height: 50,
        marginBottom: 10
    },
    items: {
        width: "50%",
    },
    screenTitle: {
        fontSize: 35,
        textAlign: 'center',
        margin: 10,
        color: "#FFF",
    },
    textInput: {
        height: 50,
        borderColor: '#FFF',
        borderWidth: 1,
        borderRadius: 3,
        backgroundColor: '#FFF',
        paddingHorizontal: 10,
        marginBottom: 10,
        fontSize: 18,
        color: '#3F4EA5',
        width: "80%",
    },
    button: {
        backgroundColor: '#D4AF37',
        borderRadius: 5,
        height: 45,
        width: "80%",
        marginTop: 10,
        marginBottom: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonText: {
        color: '#FFF',
        fontWeight: 'bold',
        fontSize: 16,
    },
});

export default LocationScreen;
