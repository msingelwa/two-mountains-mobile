import React, {Component, useState} from 'react';
import {Image, SafeAreaView, StyleSheet, TouchableOpacity,TextInput, View} from "react-native";
import {Text} from "../components/Themed";
import EditScreenInfo from "../components/EditScreenInfo";
import {StatusBar} from "expo-status-bar";
import {Button, } from "react-native-paper";
import {Ionicons} from "@expo/vector-icons";
import {Actionsheet} from "native-base";

var MEDIA_OPTIONS = [
    { text: "Take a Photo", icon: "camera-outline", iconColor: "#000" },
    { text: "Open Media Library", icon: "images-outline", iconColor: "#000" },
    { text: "Cancel", icon: "close", iconColor: "#000" },
];
var DESTRUCTIVE_INDEX = 2;
var CANCEL_INDEX = 3;
function CommentModalScreen(){

    const [description, setDescription] = useState("");
    const [image, setImage] = useState("");

    function submitComment(description: any, image: any) {

    }
    return (
            <SafeAreaView style={styles.container}>
                <View style={styles.textInput}>

                    <TextInput
                        multiline={true}
                        numberOfLines={5}
                        onChangeText={(title: any) => setDescription(title)}
                        value={description}
                        scrollEnabled={true}
                    />
                </View>
                <View style={styles.image}>
                    <TouchableOpacity
                        onPress={() => {}}>
                        <Ionicons name="camera-outline" size={35} />
                    </TouchableOpacity>
                </View>
                {/** From Modal to Action Sheet **/}

                <View style={{ backgroundColor: "3ac68f" }}>
                    {image ? (
                        <Image
                            style={{
                                width: "100%",
                                height: 100,
                                marginBottom: 5,
                            }}
                            source={{ uri: image }}
                        ></Image>
                    ) : null}
                </View>
                {/*<View style={styles.button}>*/}
                {/*    <Button*/}
                {/*        color="#8baaf7"*/}
                {/*        onPress={() => {*/}
                {/*            submitComment(description, image);*/}
                {/*        }}*/}
                {/*    >*/}
                {/*        <Text>Submit</Text>*/}
                {/*    </Button>*/}
                {/*</View>*/}
            </SafeAreaView>
        );
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    button: {
        backgroundColor: '#96C760',
        borderRadius: 5,
        height: 45,
        width: "80%",
        marginTop: 5,
        marginBottom: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonText: {
        color: '#FFF',
        fontWeight: 'bold',
        fontSize: 16,
    },
    separator: {
        marginVertical: 30,
        height: 1,
        width: '80%',
    }, textInput: {
        borderColor: "#111"
    },
    image:{

    }
});
export default CommentModalScreen;
