import React, {useState} from 'react';
import {
    ActivityIndicator,
    Alert,
    Picker,
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View
} from "react-native";
import RadioGroup, {RadioButtonProps} from "react-native-radio-buttons-group/lib/index";
import firebase, {firestore} from "../config/firebase";
import { AsyncStorage } from "react-native";
import BodyDeclarationScreen from "./BodyDeclarationScreen";
import {FormControl, Input, Stack} from "native-base";
import {getDatabase, push, ref} from "firebase/database";


const radioButtonsData = [{
    id: '1', // acts as primary key, should be unique and non-empty string
    label: 'Home',
    value: 'Home Address'
},{
    id: '2', // acts as primary key, should be unique and non-empty string
    label: 'Hospital',
    value: 'Hospital Name'
},{
    id: '3', // acts as primary key, should be unique and non-empty string
    label: 'Scene',
    value: 'Scene'
}]

function NewCollectionScreen({navigation}:any) {
    const [isLoading, setLoading] = useState(false);
    const [name, setName] = useState("");
    const [surname, setSurname] = useState("");
    const [time, setTime] = useState(new Date);
    const [idNo, setIdNo] = useState("");
    const [collectedFrom, setCollectedFrom] = useState("");
    const [informantName, setInformantName] = useState("");
    const [informant, setInformant] = useState("");
    const [informantNumber, setInformantNumber] = useState("");
    const [radioButtons6, setRadioButtons6] = useState<RadioButtonProps[]>(radioButtonsData)
    const [data, setData] = useState({})
    const [collectedFromPlaceholder, setCollectedFromPlaceholder] = useState('Home Address')

    const handleSubmit = async () => {
        setLoading(true);
        let timeNow = time.toLocaleTimeString();
        const data : any =  {
            name,
            surname,
            idNo,
            collectedFrom,
            informant,
            informantNumber,
            timeNow
        }
        try {
            setLoading(false);
            navigation.navigate('BodyDeclaration', {data: data})
        }catch (e) {
            console.log(e)
        }

    }

    const h = () => {
        let timeNow = time.toLocaleTimeString();

        firestore()
            .collection('collections')
            .add({
                name,
                surname,
                idNo,
                collectedFrom,
                informant,
                informantNumber,
                timeNow
            })
            .then((res: any) => {
                navigation.navigate('BodyDeclaration', {data: res})
            })
            .catch((error :any) => {
                console.log(error)
            });
    }

    // const resetForm = () => {
    //     setName("");
    //     setSurname("");
    // }

    function onPressRadioButton(radios: RadioButtonProps[]) {
        radios.forEach(t => {
            if (t.selected){
                setCollectedFromPlaceholder(t.value as string);
            }
        });
    }


    return (
        <>
            {/*<StatusBar barStyle="dark-content" backgroundColor="#2f95dc" animated={true}/>*/}
            <ScrollView>
            <View style={styles.container}>
                <TextInput
                    style={styles.textInput}
                    placeholder="Name"
                    keyboardType="default"
                    returnKeyType="done"
                    blurOnSubmit
                    onChangeText={text => setName(text)}
                    value={name}
                />
                <TextInput
                    style={styles.textInput}
                    placeholder="Surname"
                    keyboardType="default"
                    returnKeyType="done"
                    blurOnSubmit
                    onChangeText={text => setSurname(text)}
                    value={surname}
                />
                <TextInput
                    style={styles.textInput}
                    placeholder="Identity No"
                    keyboardType="numeric"
                    returnKeyType="done"
                    blurOnSubmit
                    onChangeText={value => setIdNo(value)}
                    value={idNo}
                />
                <RadioGroup
                    layout={"row"}
                    radioButtons={radioButtons6}
                    onPress={value => onPressRadioButton(value)}
                />

                <TextInput
                    style={styles.textInput}
                    placeholder={collectedFromPlaceholder}
                    keyboardType="default"
                    returnKeyType="done"
                    blurOnSubmit
                    onChangeText={text => setCollectedFrom(text)}
                    value={collectedFrom}
                />
                <Text style={{
                    marginBottom: 5,
                    paddingHorizontal: 10,
                    fontSize: 18,
                    justifyContent: 'flex-start',
                    alignItems: 'flex-start',
                    }}>Informant/Family Member</Text>
                <TextInput
                    style={styles.textInput}
                    placeholder="Name"
                    keyboardType="default"
                    returnKeyType="done"
                    blurOnSubmit
                    onChangeText={text => setInformantName(text)}
                    value={informantName}
                /><TextInput
                    style={styles.textInput}
                    placeholder="Surname"
                    keyboardType="default"
                    returnKeyType="done"
                    blurOnSubmit
                    onChangeText={text => setInformant(text)}
                    value={informant}
                /><TextInput
                    style={styles.textInput}
                    placeholder="Contact Number"
                    keyboardType="numeric"
                    returnKeyType="done"
                    blurOnSubmit
                    onChangeText={text => setInformantNumber(text)}
                    value={informantNumber}
                />

                <TouchableOpacity style={styles.button} onPress={handleSubmit}>
                    <Text style={styles.buttonText}>Next</Text>
                </TouchableOpacity>
                {
                    isLoading ? (
                        <View style={styles.loader}>
                            <ActivityIndicator size={"large"} color="#777535"/>
                        </View>
                    ) : undefined
                }

                {/*<TouchableOpacity style={styles.button} onPress={resetForm}>*/}
                {/*    <Text style={styles.buttonText}>Reset</Text>*/}
                {/*</TouchableOpacity>*/}
            </View>
            </ScrollView>
        </>
        );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        top: 15,
        alignItems: 'center',
        justifyContent: 'center',
        // paddingHorizontal: 5
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
    },
    collected: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'flex-start',
        height: 50,
        marginBottom: 10
    },
    items: {
            width: "50%",
        },
    screenTitle: {
        fontSize: 35,
        textAlign: 'center',
        margin: 10,
        color: "#FFF",
    },
    textInput: {
        height: 50,
        borderColor: '#FFF',
        borderWidth: 1,
        borderRadius: 3,
        backgroundColor: '#FFF',
        paddingHorizontal: 10,
        marginBottom: 10,
        fontSize: 18,
        color: '#3F4EA5',
        width: "90%",
    },
    button: {
        backgroundColor: '#D4AF37',
        borderRadius: 5,
        height: 45,
        width: "90%",
        marginTop: 10,
        marginBottom: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonText: {
        color: '#FFF',
        fontWeight: 'bold',
        fontSize: 16,
    },loader: {
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center',
        color: "#D4AF37",
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
    },
});

export default NewCollectionScreen;
