import React from 'react';
import {AsyncStorage, StyleSheet,Text, TouchableOpacity, View} from "react-native";

function SignPadScreen({navigation, route } : any) {

    const data = route.params.data;

    const handleSubmit = async () => {
        navigation.navigate('Summary', { data: data });
    }

    const signaturePadError = (error: any) => {
        console.error(error);
    };

    const signaturePadChange = ({base64DataUrl}: any) => {
        console.log("Got new signature: " + base64DataUrl);
    };
        return (
            <View style={styles.container}>

                <View style={styles.boxSimple}>
                    <Text>
                        By signing here/below, the family member /informant confirms
                        that the information provided/above is
                        true(please sign in the signature pad)
                    </Text>
                </View>
                <TouchableOpacity style={styles.button} onPress={() => handleSubmit()}>
                    <Text style={styles.buttonText}>Next</Text>
                </TouchableOpacity>
            </View>
        );
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 10,
        flexDirection: 'row',
        flexWrap: 'wrap',
        // alignItems: 'center',
        paddingLeft: 20,
        backgroundColor: "#ffffff"
    },
    boxSimple: {
        backgroundColor: '#fff',
        borderRadius: 4,
        borderWidth: 0.5,
        borderColor: '#000',
        width: '85%',
        height: 120,
        padding: 10,
        margin: 20,
    },
    inside: {
        flex: 2,
        flexDirection: 'row'
    },
    items: {
        width: "30%"
    },item: {
        width: "60%",
        paddingHorizontal: 10
    },
    button: {
        backgroundColor: '#D4AF37',
        borderRadius: 5,
        height: 45,
        width: "100%",
        marginTop: 5,
        marginBottom: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonText: {
        color: '#FFF',
        fontWeight: 'bold',
        fontSize: 16,
    },
    text: {
        padding: 20,
        fontSize: 20,
    }
});
export default SignPadScreen;
