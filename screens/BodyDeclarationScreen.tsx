import React, {Component, useEffect, useState} from 'react';
import {
    View,
    Text,
    StyleSheet,
    Picker,
    TouchableOpacity,
    Platform,
    AsyncStorage,
    ActivityIndicator
} from "react-native";
import RadioGroup, { RadioButtonProps } from 'react-native-radio-buttons-group';

import * as ImagePicker from "expo-image-picker";
import {radioButtonsData} from "../data/dummy";
import UpperBodyScreen from "./UpperBodyScreen";

var DESTRUCTIVE_INDEX = 2;
var CANCEL_INDEX = 3;

function BodyDeclarationScreen({navigation, route }: any) {
    const [radioButtons, setRadioButtons] = useState<RadioButtonProps[]>(radioButtonsData)
    const [radioButtons1, setRadioButtons1] = useState<RadioButtonProps[]>(radioButtonsData)
    const [radioButtons2, setRadioButtons2] = useState<RadioButtonProps[]>(radioButtonsData)
    const [radioButtons3, setRadioButtons3] = useState<RadioButtonProps[]>(radioButtonsData)
    const [radioButtons4, setRadioButtons4] = useState<RadioButtonProps[]>(radioButtonsData)
    const [isLoading, setLoading] = useState(false);
    const data  = route.params.data;

    let nose: any;
    let mouth : any;
    let eyes: any ;
    let ears : any;
    let face : any;

    function onPressRadioButton1(radios: RadioButtonProps[]) {
        radioButtons.map((m, i) => {
            if (m.selected){
                nose = m.value;
            }
        })
    }function onPressRadioButton2(radios: RadioButtonProps[]) {
        radioButtons1.map((m, i) => {
            if (m.selected){
               mouth = m.value
            }
        })
    }function onPressRadioButton3(radios: RadioButtonProps[]) {
        radioButtons2.map((m, i) => {
            if (m.selected){
                eyes = m.value
            }
        })
    }function onPressRadioButton4(radios: RadioButtonProps[]) {
        radioButtons3.map((m, i) => {
            if (m.selected){
                ears = m.value
            }
        })
    }function onPressRadioButton5(radios: RadioButtonProps[]) {
        radioButtons4.map((m, i) => {
            if (m.selected){
                face = m.value
            }
        })
    }
    const handleSubmit = async () => {
        setLoading(true);
        const upper : any =  {
            nose,
            mouth,
            eyes,
            ears,
            face,
        }
        try {
           const newCollection = {...data, ...upper};
            await AsyncStorage.setItem('collection', JSON.stringify(data));
            navigation.navigate('UpperBody', {data: newCollection});
            setLoading(false);
        } catch (error) {
            // Error saving data
            console.log(error)
        }
    }

    {
        return (
            <>
                <Text style={styles.text}>Head</Text>
            <View style={styles.container}>
                <View style={styles.items}>
                    <Text style={styles.text}>Nose</Text>
                    <Text style={styles.text}>Mouth</Text>
                    <Text style={styles.text}>Eyes</Text>
                    <Text style={styles.text}>Ears</Text>
                    <Text style={styles.text}>Face</Text>
                </View>
                <View style={styles.items}>
                    <View style={styles.inside}>
                        <View style={[styles.item]}>
                            <RadioGroup

                                layout={"row"}
                                radioButtons={radioButtons}
                                onPress={value => onPressRadioButton1(value)}
                            />
                        </View>
                        </View>
                    <View style={styles.inside}>
                        <View style={[styles.item]}>
                            <RadioGroup
                                layout={"row"}
                                radioButtons={radioButtons1}
                                onPress={value => onPressRadioButton2(value)}
                            />
                        </View>
                    </View>
                    <View style={styles.inside}>
                        <View style={[styles.item]}>
                            <RadioGroup
                                layout={"row"}
                                radioButtons={radioButtons2}
                                onPress={value => onPressRadioButton3(value)}
                            />
                        </View>
                    </View><View style={styles.inside}>
                        <View style={[styles.item]}>
                            <RadioGroup
                                layout={"row"}
                                radioButtons={radioButtons3}
                                onPress={value => onPressRadioButton4(value)}
                            />
                        </View>
                    </View><View style={styles.inside}>
                        <View style={[styles.item]}>
                            <RadioGroup
                                layout={"row"}
                                radioButtons={radioButtons4}
                                onPress={value => onPressRadioButton5(value)}
                            />
                        </View>
                    </View>
                </View>
                <TouchableOpacity style={styles.button} onPress={() => handleSubmit()}>
                    <Text style={styles.buttonText}>Next</Text>
                </TouchableOpacity>
                {
                    isLoading ? (
                        <View style={styles.loader}>
                            <ActivityIndicator size={"large"} color="#777535"/>
                        </View>
                    ) : undefined
                }
            </View>
            </>
        );
    }
}
const styles = StyleSheet.create({
        loader: {
            position: 'absolute',
            alignItems: 'center',
            justifyContent: 'center',
            left: 0,
            right: 0,
            top: 0,
            bottom: 0,
        },
    container: {
        flex: 1,
        marginTop: 10,
        flexDirection: 'row',
        flexWrap: 'wrap',
        // alignItems: 'center',
        paddingLeft: 20,
        backgroundColor: "#ffffff"
    },
    inside: {
        flex: 2,
        flexDirection: 'row'
    },
    items: {
        width: "30%"
    },item: {
        width: "60%",
        paddingHorizontal: 10
    },
    button: {
        backgroundColor: '#D4AF37',
        borderRadius: 5,
        height: 45,
        width: "100%",
        marginTop: 5,
        marginBottom: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonText: {
        color: '#FFF',
        fontWeight: 'bold',
        fontSize: 16,
    },
    text: {
        padding: 20,
        fontSize: 20
        }
});
export default BodyDeclarationScreen;
