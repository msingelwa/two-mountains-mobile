import React, {Component, useEffect, useState} from 'react';
import {
    Alert,
    Text,
    TouchableOpacity,
    View,
    StyleSheet,
    Image,
    FlatList,
    ActivityIndicator,
    ScrollView, RefreshControl
} from "react-native";
import {FAB} from "react-native-paper";
import {Button, colors, Icon, ListItem} from "react-native-elements";
import {Ionicons} from "@expo/vector-icons";
import {getDatabase, onValue, ref, onChildAdded} from "firebase/database";


const Item = ({title}: any) => (
    <View style={styles.item}>
        <Text style={styles.title}>{title}</Text>
    </View>
);

const wait = (timeout: any) => {
    return new Promise(resolve => setTimeout(resolve, timeout));
}

function CollectionScreen({navigation}: any) {
    const [list, setList] = useState<any[]>([]);
    const [isLoading, setIsLoading] = useState(true);
    const db = getDatabase();
    const reference = ref(db, 'collections');
    const newCollections: any[] = [];

    const [refreshing, setRefreshing] = React.useState(false);

    const onRefresh = React.useCallback(() => {
        setRefreshing(true);
        // collectionlist();
        wait(2000).then(() => setRefreshing(false));
    }, []);


    useEffect(() => {
            collectionlist();
        },[]);

    const collectionlist = () => {
        onValue(reference, (snapshot) => {
            snapshot.forEach(res => {
                const data = res.val();
                data.key = res.key;
                newCollections.push(data);
                    setList( newCollections);
                setIsLoading(false);
            })

        });
    }

    const ItemSeparatorView = () => {
        return (
            // Flat List Item Separator
            <View
                style={{
                    height: 0.5,
                    width: '100%',
                    backgroundColor: '#C8C8C8'}}
            />
        );
    };

    const renderItems = (results: any) => {
        const res = results.item;
        return (
            <ListItem.Swipeable
                key={res.key}
                leftContent={
                    <Button
                        // title="Info"
                        icon={{ name: 'info', color: 'white' }}
                        buttonStyle={{ minHeight: '100%' }}
                    />
                }
                rightContent={
                    <Button
                        // title="Files"
                        icon={{ name: 'folder', color: 'white' }}
                        buttonStyle={{ minHeight: '100%', backgroundColor: 'grey' }}
                        onPress={() => {Alert.alert("To add files")}}
                    />
                }
            >
                <ListItem.Content
                >
                    <ListItem.Title style={{color:"#111"}}>{res.name}</ListItem.Title>
                    <ListItem.Title>{res.surname}</ListItem.Title>
                </ListItem.Content>
                <ListItem.Content right>
                    <ListItem.Title right style={{ color: 'green' }}>
                        {res.timeNow}
                    </ListItem.Title>
                </ListItem.Content>
                <Icon
                    name="chevron-right"
                    type="feather"
                    color='#517fa4'
                />
                {/*<ListItem.Chevron color={"red"} backgroundColor={""}/>*/}
            </ListItem.Swipeable>
        );
    }

    if (isLoading) {
        return (
            <View style={styles.loader}>
                <ActivityIndicator size={"large"} color="#777535"/>
            </View>
        );
    } else {
        return (
            <>
                {/*<View style={{padding: 2, paddingHorizontal: 10, borderRadius: 10}}>*/}
                    <FlatList
                        style={styles.flatList}
                        data={list}
                        keyExtractor={(res, index) => index.toString()}
                        // ItemSeparatorComponent={ItemSeparatorView}
                        // enableEmptySections={true}
                        renderItem={renderItems}
                        refreshControl={
                            <RefreshControl
                                //refresh control used for the Pull to Refresh
                                refreshing={refreshing}
                                onRefresh={onRefresh}
                            />
                        }

                    />
                {/*</View>*/}
                <FAB
                    style={styles.fab}
                    meduim
                    icon="plus"
                    onPress={() => {
                        navigation.navigate("NewCollection")
                    }}
                />
            </>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: "#e5e5e5"
    },
    wrapper: {
        flex: 1,
        paddingBottom: 22,
    },
    flatList: {
        margin: 2,
        borderRadius: 10,
        padding: 5,
    },
    loader: {
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
    },
    headerText: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
        fontWeight: 'bold'
    },
    TouchableOpacityStyle: {
        position: 'absolute',
        width: 50,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        right: 30,
        bottom: 30,
    },
    FloatingButtonStyle: {
        resizeMode: 'contain',
        width: 50,
        height: 50,
        justifyContent: 'center',
    },
    item: {
        backgroundColor: '#96C760',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
        borderRadius: 10,
    },
    title: {
        fontSize: 20,
    },
    fab: {
        position: 'absolute',
        margin: 16,
        right: 0,
        bottom: 0,
        backgroundColor: "#777535"
    },
});


export default CollectionScreen;
