/**
 * If you are not familiar with React Navigation, refer to the "Fundamentals" guide:
 * https://reactnavigation.org/docs/getting-started
 *
 */
import {AntDesign, FontAwesome, Ionicons} from '@expo/vector-icons';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer, DefaultTheme, DarkTheme } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import * as React from 'react';
import { ColorSchemeName, Pressable } from 'react-native';

import Colors from '../constants/Colors';
import useColorScheme from '../hooks/useColorScheme';
import ModalScreen from '../screens/ModalScreen';
import NotFoundScreen from '../screens/NotFoundScreen';
import { RootStackParamList, RootTabParamList, RootTabScreenProps } from '../types';
import LinkingConfiguration from './LinkingConfiguration';
import HomeScreen from "../screens/HomeScreen";
import CollectionScreen from "../screens/CollectionScreen";
import ProfileScreen from "../screens/ProfileScreen";
import SettingsScreen from "../screens/SettingsScreen";
import NewCollectionScreen from "../screens/NewCollectionScreen";
import BodyDeclarationScreen from "../screens/BodyDeclarationScreen";
import LocationScreen from "../screens/LocationScreen";
import UpperBodyScreen from "../screens/UpperBodyScreen";
import LowerBodyScreen from "../screens/LowerBodyScreen";
import SummaryScreen from "../screens/SummaryScreen";
import CommentModalScreen from "../screens/CommentModalScreen";
import SignPadScreen from "../screens/SignPadScreen";

export default function Navigation({ colorScheme }: { colorScheme: ColorSchemeName }) {
  return (
    <NavigationContainer
      linking={LinkingConfiguration}
      theme={colorScheme === 'dark' ? DarkTheme : DefaultTheme}>
      <RootNavigator />
    </NavigationContainer>
  );
}

/**
 * A root stack navigator is often used for displaying modals on top of all other content.
 * https://reactnavigation.org/docs/modal
 */
const Stack = createNativeStackNavigator<RootStackParamList>();

function RootNavigator() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Root" component={BottomTabNavigator} options={{ headerShown: false }} />
      <Stack.Screen name="NotFound" component={NotFoundScreen} options={{ title: 'Oops!' }} />
      <Stack.Group screenOptions={{ presentation: 'modal' }}>
        <Stack.Screen options={{ title: 'Awesome app' }} name="Modal" component={ModalScreen} />
        <Stack.Screen options={{ title: 'Comment Modal' }} name="Comment" component={CommentModalScreen} />
      </Stack.Group>
        <Stack.Screen options={{ title: 'New Collection' }} name="NewCollection" component={NewCollectionScreen} />
        <Stack.Screen options={{ title: 'Body Declaration' }} name="BodyDeclaration" component={BodyDeclarationScreen} />
        <Stack.Screen options={{ title: 'Body Declaration' }} name="UpperBody" component={UpperBodyScreen} />
        <Stack.Screen options={{ title: 'Body Declaration' }} name="LowerBody" component={LowerBodyScreen} />
        <Stack.Screen options={{ title: 'Drop Of Location' }} name="Location" component={LocationScreen} />
        <Stack.Screen options={{ title: 'Signature' }} name="Signature" component={SignPadScreen} />
        <Stack.Screen options={{ title: 'Summary' }} name="Summary" component={SummaryScreen} />

    </Stack.Navigator>
  );
}

/**
 * A bottom tab navigator displays tab buttons on the bottom of the display to switch screens.
 * https://reactnavigation.org/docs/bottom-tab-navigator
 */
const BottomTab = createBottomTabNavigator<RootTabParamList>();

function BottomTabNavigator() {
  const colorScheme = useColorScheme();

  return (
    <BottomTab.Navigator
      initialRouteName="Home"
      screenOptions={{
        tabBarActiveTintColor: Colors[colorScheme].tint,
          tabBarStyle: {
              borderTopColor: '#66666666',
              backgroundColor: '#071a1a',
              elevation: 0,
              height: 50
          },
      }}>
      <BottomTab.Screen
        name="Home"
        component={HomeScreen}
        options={({ navigation }: RootTabScreenProps<'Home'>) => ({
          title: 'Home',
          tabBarIcon: ({ color }) => <TabBarIcon name="home" color={color} />,
          headerRight: () => (
            <Pressable
              onPress={() => navigation.navigate('Modal')}
              style={({ pressed }) => ({
                opacity: pressed ? 0.5 : 1,
              })}>
              <FontAwesome
                name="info-circle"
                size={25}
                color={Colors[colorScheme].text}
                style={{ marginRight: 15 }}
              />
            </Pressable>
          ),
        })}
      />
      <BottomTab.Screen
        name="Collection"
        component={CollectionScreen}
        options={{
          title: 'Collection',
          tabBarIcon: ({ color }) => <TabBarIcon name="list" color={color} />,
        }}
      />
        <BottomTab.Screen
            name="Profile"
            component={ProfileScreen}
            options={{
                title: 'Profile',
                tabBarIcon: ({ color }) => <TabBarIcon name="person-circle" color={color} />,
            }}
        />
        <BottomTab.Screen
            name="Settings"
            component={SettingsScreen}
            options={{
                title: 'Settings',
                tabBarIcon: ({ color }) => <TabBarIcon name="settings" color={color} />,
            }}
        />
    </BottomTab.Navigator>
  );
}

/**
 * You can explore the built-in icon families and icons on the web at https://icons.expo.fyi/
 */
function TabBarIcon(props: {
  name: React.ComponentProps<typeof Ionicons>['name'];
  color: string;
}) {
  return <Ionicons size={30} style={{ marginBottom: -3 }} {...props} />;
}
