export const radioButtonsData = [{
    id: '11', // acts as primary key, should be unique and non-empty string
    label: 'Intact',
    value: 'Intact'
},{
    id: '12', // acts as primary key, should be unique and non-empty string
    label: 'Injured',
    value: 'Injured'
},{
    id: '13', // acts as primary key, should be unique and non-empty string
    label: 'Missing',
    value: 'Missing'
}]
